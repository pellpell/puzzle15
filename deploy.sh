#!/bin/bash

source ./.env

ssh-add $SSH_CONFIG

git config --local user.name $USER_NAME
git config --local user.email $USER_MAIL

rm -rf public

# build wasm
rustup target add wasm32-unknown-unknown
cargo install wasm-bindgen-cli --force
cargo build --release --target wasm32-unknown-unknown
wasm-bindgen --out-dir public --no-typescript --target web target/wasm32-unknown-unknown/release/puzzle15.wasm

cp -r assets public
cp static/* public
sed -i.bak \
    -e 's/function init(input) {/function init(customFetch, input) { customFetch = customFetch || fetch;/' \
    -e 's/input = fetch(/input = customFetch(/' \
    -e 's/getObject(arg0).fetch(/customFetch(/' \
    -e 's/const imports = getImports();/const imports = getImports(customFetch);/' \
    -e 's/function getImports() {/function getImports(customFetch) {/' \
    ./public/puzzle15.js

git add public
git commit -m "build: GitLab pages"

git push origin HEAD
