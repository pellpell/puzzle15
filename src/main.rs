pub mod base;
pub mod game;
pub mod scene;

use bevy::prelude::*;

fn main() {
    // When building for WASM, print panics to the browser console
    #[cfg(target_arch = "wasm32")]
    console_error_panic_hook::set_once();

    App::new()
        .add_plugins(DefaultPlugins)
        .add_event::<CursorMoved>()
        .add_event::<game::events::TileClickedEvent>()
        .add_event::<game::events::SlideTileEvent>()
        .add_event::<game::events::ToClearStateEvent>()
        .init_resource::<base::resources::CursorState>()
        .init_resource::<game::resources::TileState>()
        .init_resource::<game::resources::XCenter>()
        .init_resource::<game::resources::YCenter>()
        .init_resource::<game::resources::XChecker>()
        .init_resource::<game::resources::YChecker>()
        .init_resource::<game::resources::EmptyTile>()
        .add_state(scene::states::AppState::Game)
        .add_startup_system(setup)
        .add_system(base::systems::set_coursor_state)
        .add_startup_system(base::systems::setup_sign)
        .add_system_set(
            SystemSet::on_enter(scene::states::AppState::Start)
                .with_system(scene::systems::start_on_enter),
        )
        .add_system_set(
            SystemSet::on_enter(scene::states::AppState::Game)
                .with_system(game::systems::setup_stage),
        )
        .add_system_set(
            SystemSet::on_enter(scene::states::AppState::Game)
                .with_system(game::systems::setup_tiles),
        )
        .add_system_set(
            SystemSet::on_update(scene::states::AppState::Game)
                .with_system(game::systems::check_tile_clicked)
                .label("check_tile_clicked"),
        )
        .add_system_set(
            SystemSet::on_update(scene::states::AppState::Game)
                .with_system(game::systems::calc_slide_tile)
                .label("calc_slide_tile")
                .after("check_tile_clicked"),
        )
        .add_system_set(
            SystemSet::on_update(scene::states::AppState::Game)
                .with_system(game::systems::slide_tile)
                .label("slide_tile")
                .after("calc_slide_tile"),
        )
        // .add_system_set(
        //     SystemSet::on_update(scene::states::AppState::Game)
        //         .with_system(game::systems::tile_se)
        //         .label("tile_se")
        //         .after("calc_slide_tile"),
        // )
        .add_system_set(
            SystemSet::on_update(scene::states::AppState::Game)
                .with_system(game::systems::to_clear_state)
                .label("to_clear_state")
                .after("slide_tile"),
        )
        // Clear
        .add_system_set(
            SystemSet::on_enter(scene::states::AppState::Clear)
                .with_system(scene::clear::systems::setup_stage),
        )
        .run();
}

fn setup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());
}
