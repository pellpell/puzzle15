// タイルについて

// タイルの個数
pub(crate) const ROWS: i32 = 4;
pub(crate) const COLUMNS: i32 = 4;

// 1つのタイルの幅と高さ
pub(crate) const TILE_WIDTH: f32 = 120.0;
pub(crate) const TILE_HEIGHT: f32 = TILE_WIDTH;

// タイルの間隔(タイルの幅と高さは考慮しない)
pub(crate) const INTERVAL_WIDTH: f32 = 125.0;
pub(crate) const INTERVAL_HEIGHT: f32 = INTERVAL_WIDTH;

// タイルたちを一つにみたときの横幅・高さ
pub(crate) const TILES_WIDTH: f32 = INTERVAL_WIDTH * ROWS as f32;
pub(crate) const TILES_HEIGHT: f32 = INTERVAL_HEIGHT * COLUMNS as f32;

pub(crate) const TILE_IMAGE_WIDTH: f32 = 100.0;
pub(crate) const TILE_IMAGE_HEIGHT: f32 = 100.0;

pub(crate) const X_START: f32 = -TILES_WIDTH / 2.0 + INTERVAL_WIDTH / 2.0;
pub(crate) const Y_START: f32 = TILES_HEIGHT / 2.0 - INTERVAL_HEIGHT / 2.0;

// pub(crate) const TILE_COLOR: (u8, u8, u8) = (255, 229, 127);

// ステージについて

pub(crate) const STAGE_WIDTH: f32 = 550.0;
pub(crate) const STAGE_HEIGHT: f32 = STAGE_WIDTH;

pub(crate) const STAGE_IN_INTERVAL_WIDTH: f32 =
    STAGE_WIDTH - TILES_WIDTH + (INTERVAL_WIDTH - TILE_WIDTH);
pub(crate) const STAGE_IN_INTERVAL_HEIGHT: f32 =
    STAGE_HEIGHT - TILES_HEIGHT + (INTERVAL_HEIGHT - TILE_HEIGHT);

pub(crate) const STAGE_COLOR: (u8, u8, u8) = (62, 39, 35);
pub(crate) const STAGE_IN_COLOR: (u8, u8, u8) = (255, 248, 225);

pub(crate) const CORRECT_TILES: [i32; (ROWS * COLUMNS) as usize] =
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
