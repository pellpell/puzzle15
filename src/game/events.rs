use bevy::prelude::*;

pub(crate) struct TileClickedEvent(pub(crate) Vec2);

pub(crate) struct SlideTileEvent(pub(crate) Vec2, pub(crate) Entity);

pub(crate) struct ToClearStateEvent();
