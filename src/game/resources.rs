use bevy::prelude::*;
use rand::prelude::*;

use crate::game::logic::parity_check::parity_check;

use super::consts::*;

pub(crate) struct TileState(pub(crate) [[(i32, Option<Entity>); COLUMNS as usize]; ROWS as usize]);

// TileState の初期化
impl FromWorld for TileState {
    fn from_world(_: &mut World) -> Self {
        let mut rng = rand::thread_rng();
        let mut numbers: Vec<i32> = (1..=(COLUMNS * ROWS)).collect();
        while numbers == CORRECT_TILES || !parity_check(numbers.clone()) {
            numbers.shuffle(&mut rng);
        }

        let mut array: [[(i32, Option<Entity>); COLUMNS as usize]; ROWS as usize] =
            [[(0, None); COLUMNS as usize]; ROWS as usize];
        let mut i: usize = 0;
        for y in 0..COLUMNS {
            for x in 0..ROWS {
                array[y as usize][x as usize].0 = numbers[i];
                i += 1;
            }
        }
        // let empty = array[3][3];
        // array[3][3] = array[2][3];
        // array[2][3] = empty;

        println!("array = {:?}", array);
        TileState(array)
    }
}

pub(crate) struct XCenter(pub(crate) [f32; COLUMNS as usize]);

// XCenter の初期化
impl FromWorld for XCenter {
    fn from_world(_: &mut World) -> Self {
        let mut array: [f32; COLUMNS as usize] = [0.0; COLUMNS as usize];
        let mut x = X_START;
        for a in array.iter_mut() {
            *a = x;
            x += INTERVAL_WIDTH;
        }
        println!("x_center = {:?}", array);
        XCenter(array)
    }
}

pub(crate) struct YCenter(pub(crate) [f32; COLUMNS as usize]);

// YCenter の初期化
impl FromWorld for YCenter {
    fn from_world(_: &mut World) -> Self {
        let mut array: [f32; COLUMNS as usize] = [0.0; COLUMNS as usize];
        let mut y = Y_START;
        for a in array.iter_mut() {
            *a = y;
            y -= INTERVAL_HEIGHT;
        }
        println!("y_center = {:?}", array);
        YCenter(array)
    }
}

pub(crate) struct XChecker(pub(crate) [(f32, f32); COLUMNS as usize]);

// XChecker の初期化
impl FromWorld for XChecker {
    fn from_world(_: &mut World) -> Self {
        let mut array: [(f32, f32); COLUMNS as usize] = [(0.0, 0.0); COLUMNS as usize];
        let mut x = X_START - TILE_WIDTH / 2.0;
        for (a, b) in array.iter_mut() {
            *a = x;
            *b = x + TILE_WIDTH;
            x += INTERVAL_WIDTH;
        }
        println!("xchecker = {:?}", array);
        XChecker(array)
    }
}

impl XChecker {
    pub(crate) fn calc_index(&self, x: &f32) -> Option<usize> {
        for (i, (a, b)) in self.0.iter().enumerate() {
            if x >= a && x <= b {
                return Some(i);
            }
        }
        None
    }
}

pub(crate) struct YChecker(pub(crate) [(f32, f32); COLUMNS as usize]);

// YChecker の初期化
impl FromWorld for YChecker {
    fn from_world(_: &mut World) -> Self {
        let mut array: [(f32, f32); COLUMNS as usize] = [(0.0, 0.0); COLUMNS as usize];
        let mut y = Y_START + TILE_HEIGHT / 2.0;
        for (a, b) in array.iter_mut() {
            *a = y;
            *b = y - TILE_HEIGHT;
            y -= INTERVAL_HEIGHT;
        }
        println!("ychecker = {:?}", array);
        YChecker(array)
    }
}

impl YChecker {
    pub(crate) fn calc_index(&self, y: &f32) -> Option<usize> {
        for (i, (a, b)) in self.0.iter().enumerate() {
            if y <= a && y >= b {
                return Some(i);
            }
        }
        None
    }
}

pub(crate) struct EmptyTile(pub(crate) Vec2);

// EmptyTile の初期化
impl FromWorld for EmptyTile {
    fn from_world(_: &mut World) -> Self {
        EmptyTile(Vec2::new((ROWS - 1) as f32, (COLUMNS - 1) as f32))
    }
}
