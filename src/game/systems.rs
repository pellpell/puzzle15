use bevy::prelude::*;

use crate::base::resources::CursorState;
use crate::scene::states::AppState;

use super::components::*;
use super::consts::*;
use super::events::*;
use super::resources::*;

// setup

pub(crate) fn setup_stage(mut commands: Commands) {
    let stage_width = STAGE_WIDTH;
    let stage_height = STAGE_HEIGHT;

    commands.spawn_bundle(SpriteBundle {
        sprite: Sprite {
            color: Color::rgb_u8(STAGE_COLOR.0, STAGE_COLOR.1, STAGE_COLOR.2),
            custom_size: Some(Vec2::new(stage_width, stage_height)),
            ..default()
        },
        ..default()
    });
    commands.spawn_bundle(SpriteBundle {
        sprite: Sprite {
            color: Color::rgb_u8(STAGE_IN_COLOR.0, STAGE_IN_COLOR.1, STAGE_IN_COLOR.2),
            custom_size: Some(Vec2::new(
                stage_width - STAGE_IN_INTERVAL_WIDTH,
                stage_height - STAGE_IN_INTERVAL_HEIGHT,
            )),
            ..default()
        },
        transform: Transform::from_xyz(0.0, 0.0, 0.1),
        ..default()
    });
}

pub(crate) fn setup_tiles(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    x_center: Res<XCenter>,
    y_center: Res<YCenter>,
    mut tile_state: ResMut<TileState>,
    mut empty_tile: ResMut<EmptyTile>,
) {
    let tile_width = TILE_WIDTH;
    let tile_height = TILE_HEIGHT;
    let tile_image_width = TILE_IMAGE_WIDTH;
    let tile_image_height = TILE_IMAGE_HEIGHT;

    for (j, (tile_y, y_c)) in tile_state.0.iter_mut().zip(y_center.0.iter()).enumerate() {
        for (i, (tile_x, x_c)) in tile_y.iter_mut().zip(x_center.0.iter()).enumerate() {
            if tile_x.0 != ROWS * COLUMNS {
                let path = format!("images/numbers/out-{}.png", tile_x.0).to_string();
                let id = commands
                    .spawn()
                    .insert(Tile(tile_x.0))
                    .insert_bundle(SpriteBundle {
                        texture: asset_server.load(&path),
                        transform: Transform::from_xyz(*x_c, *y_c, 0.2).with_scale(Vec3::new(
                            tile_width / tile_image_width,
                            tile_height / tile_image_height,
                            1.0,
                        )),
                        ..default()
                    })
                    .id();
                tile_x.1 = Some(id);
            } else {
                empty_tile.0 = Vec2::new(i as f32, j as f32);
            }
        }
    }
    println!("tile_state: {:?}", tile_state.0);
    println!("empty_tile: {:?}", empty_tile.0);
}

// update

pub(crate) fn check_tile_clicked(
    mut event_writer: EventWriter<TileClickedEvent>,
    cursor_state: Res<CursorState>,
    x_checker: Res<XChecker>,
    y_checker: Res<YChecker>,
) {
    if cursor_state.is_left_pressed {
        let cursor_position = &cursor_state.position;
        if let Some(y_index) = y_checker.calc_index(&cursor_position.y) {
            if let Some(x_index) = x_checker.calc_index(&cursor_position.x) {
                event_writer.send(TileClickedEvent(Vec2::new(x_index as f32, y_index as f32)));
            }
        }
    }
}

pub(crate) fn calc_slide_tile(
    mut event_reader: EventReader<TileClickedEvent>,
    mut event_writer: EventWriter<SlideTileEvent>,
    empty_tile: Res<EmptyTile>,
    tile_state: Res<TileState>,
) {
    if let Some(event) = event_reader.iter().last() {
        if (event.0 - empty_tile.0).length() == 1.0 {
            let x = event.0.x as usize;
            let y = event.0.y as usize;
            let tile = tile_state.0[y][x];
            if let Some(entity) = tile.1 {
                event_writer.send(SlideTileEvent(event.0, entity));
            }
        }
    }
}

pub(crate) fn slide_tile(
    mut event_reader: EventReader<SlideTileEvent>,
    mut event_writer: EventWriter<ToClearStateEvent>,
    x_center: Res<XCenter>,
    y_center: Res<YCenter>,
    mut empty_tile: ResMut<EmptyTile>,
    mut tile_state: ResMut<TileState>,
    mut query: Query<&mut Transform, With<Tile>>,
) {
    if let Some(event) = event_reader.iter().last() {
        if let Ok(mut transform) = query.get_mut(event.1) {
            let x = event.0.x as usize;
            let y = event.0.y as usize;

            let empty_x = empty_tile.0.x as usize;
            let empty_y = empty_tile.0.y as usize;
            transform.translation = Vec3::new(
                x_center.0[empty_x],
                y_center.0[empty_y],
                transform.translation.z,
            );

            empty_tile.0 = Vec2::new(x as f32, y as f32);

            let tmp = tile_state.0[empty_y][empty_x];
            tile_state.0[empty_y][empty_x] = tile_state.0[y][x];
            tile_state.0[y][x] = tmp;

            if check_correct_tiles(&tile_state) {
                event_writer.send(ToClearStateEvent());
            }
        }
    }
}

// pub(crate) fn tile_se(
//     mut event_reader: EventReader<SlideTileEvent>,
//     asset_server: Res<AssetServer>,
//     audio: Res<Audio>,
// ) {
//     if let Some(_) = event_reader.iter().last() {
//         let music = asset_server.load("sounds/tile_se.ogg");
//         audio.play(music);
//     }
// }

fn check_correct_tiles(tile_state: &TileState) -> bool {
    let mut i = 0;
    for y_tiles in tile_state.0 {
        for (x, _) in y_tiles {
            if x != CORRECT_TILES[i] {
                return false;
            }
            i += 1;
        }
    }
    true
}

pub(crate) fn to_clear_state(
    mut event_reader: EventReader<ToClearStateEvent>,
    mut app_state: ResMut<State<AppState>>,
) {
    if let Some(_) = event_reader.iter().last() {
        app_state.set(AppState::Clear).unwrap();
    }
}
