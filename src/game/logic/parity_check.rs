use crate::game::consts::*;

// 参考: https://manabitimes.jp/math/979
pub(crate) fn parity_check(numbers: Vec<i32>) -> bool {
    // 空きタイルの最短距離
    let index = numbers.iter().position(|x| *x == ROWS * COLUMNS).unwrap() as i32;
    let empty_min_distance = (ROWS - (index / ROWS) - 1) + (COLUMNS - (index % COLUMNS) - 1);

    // 置換の回数
    let mut numbers = numbers;
    let mut transposition_count = 0;
    for x in 0..(ROWS * COLUMNS) {
        let i = x as usize;
        while numbers[i] != CORRECT_TILES[i] {
            let j = (numbers[i] - 1) as usize;
            (numbers[i], numbers[j]) = (numbers[j], numbers[i]);
            transposition_count += 1;
        }
    }

    empty_min_distance % 2 == transposition_count % 2
}
