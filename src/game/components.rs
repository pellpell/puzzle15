use bevy::prelude::*;

#[derive(Component)]
pub(crate) struct Tile(pub(crate) i32);
