use bevy::prelude::*;

// 参考: https://qiita.com/K2Da/items/593611ea8ec1e4229d64
#[derive(Default, Debug)]
pub(crate) struct CursorState {
    pub(crate) screen_size: Vec2,
    pub(crate) position: Vec2,
    pub(crate) is_left_pressed: bool,
}
