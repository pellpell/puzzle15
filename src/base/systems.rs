use bevy::prelude::*;

use super::resources::*;

// 参考: https://qiita.com/K2Da/items/593611ea8ec1e4229d64
pub(crate) fn set_coursor_state(
    mouse_input: Res<Input<MouseButton>>,
    windows: Res<Windows>,
    mut cursor_state: ResMut<CursorState>,
    mut reader: EventReader<CursorMoved>,
) {
    for _ in reader.iter() {
        if let Some(window) = windows.iter().last() {
            cursor_state.screen_size = Vec2::new(window.width(), window.height());
            if let Some(cursor_position) = window.cursor_position() {
                cursor_state.position = cursor_position - cursor_state.screen_size / 2.0;
            }
            cursor_state.is_left_pressed = mouse_input.pressed(MouseButton::Left);
        }
    }
}

pub(crate) fn setup_sign(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn_bundle(ImageBundle {
        style: Style {
            align_self: AlignSelf::FlexEnd,
            position_type: PositionType::Absolute,
            size: Size::new(Val::Px(150.0), Val::Px(50.0)),
            position: Rect {
                bottom: Val::Px(5.0),
                right: Val::Px(15.0),
                ..default()
            },
            ..default()
        },
        image: asset_server.load("images/pellpell_sign.png").into(),
        ..default()
    });
}
