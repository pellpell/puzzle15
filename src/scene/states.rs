#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub(crate) enum AppState {
    Start,
    Game,
    Clear,
}
