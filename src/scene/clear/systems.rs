use bevy::prelude::*;

use crate::base::resources::CursorState;

use super::consts::*;

pub(crate) fn setup_stage(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    cursor_state: Res<CursorState>,
) {
    commands.spawn_bundle(SpriteBundle {
        sprite: Sprite {
            color: Color::rgba_u8(
                CLEAR_BG_COLOR[0],
                CLEAR_BG_COLOR[1],
                CLEAR_BG_COLOR[2],
                CLEAR_BG_COLOR[3],
            ),
            custom_size: Some(Vec2::new(
                cursor_state.screen_size.x,
                cursor_state.screen_size.y,
            )),
            ..default()
        },
        transform: Transform::from_xyz(0.0, 0.0, 0.4),
        ..default()
    });

    let scale = CLEAR_IMAGE_VIEW_WIDTH as f32 / CLEAR_IMAGE_WIDTH as f32;

    commands.spawn().insert_bundle(SpriteBundle {
        texture: asset_server.load(CLEAR_IMAGE_PATH),
        transform: Transform::from_xyz(0.0, 50.0, 0.5).with_scale(Vec3::new(scale, scale, 1.0)),
        ..default()
    });
}
