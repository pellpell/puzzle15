pub(crate) const CLEAR_IMAGE_PATH: &str = "images/clear.png";
pub(crate) const CLEAR_IMAGE_WIDTH: i32 = 600;

pub(crate) const CLEAR_IMAGE_VIEW_WIDTH: i32 = 750;

pub(crate) const CLEAR_BG_COLOR: [u8; 4] = [255, 248, 225, 150];
